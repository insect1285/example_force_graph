import React, {Component} from 'react';
import logo from './logo.svg';
import './App.css';

import {ForceGraph2D} from 'react-force-graph'

// Build random nodes and links with random value on the link for particles
function genRandomTree(N = 300) {
  return {
    nodes: [...Array(N).keys()].map(i => ({id: i})),
    links: [...Array(N).keys()]
      .filter(id => id)
      .map(id => ({
        source: id,
        target: Math.round(Math.random() * (id - 1)),
        value: Math.round(Math.random() * 30)/10
      }))
  };
}

// Create Image Element using an SVG
const img = document.createElement('img');
img.src = logo;
const imgSize = 20;

// Events
const onNodeClick = (node) => {
  console.log(`Clicked ${node.id}`);
};

const onNodeDragEnd = (node) => {
  console.log(`Moved node to ${node.x}, ${node.y}`);
};

class App extends Component {

  state = {
    data: genRandomTree(50),
    highlightNodes: [],
    highlightLink: null
  };

  static NODE_R = 8;

  _handleNodeHover = node => {
    this.setState({highlightNodes: node ? [node] : []});
  };
  _handleLinkHover = link => {
    this.setState({
      highlightLink: link,
      highlightNodes: link ? [link.source, link.target] : []
    });
  };
  _paintNode = (node, ctx) => {
    const {NODE_R} = App;
    const {highlightNodes} = this.state;
    if (highlightNodes.indexOf(node) !== -1) { // add ring
      ctx.beginPath();
      ctx.arc(node.x, node.y, NODE_R * 1.2, 0, 2 * Math.PI, false);
      ctx.fillStyle = 'red';
      ctx.fill();
    }
    ctx.drawImage(img, node.x - imgSize / 2, node.y - imgSize / 2, imgSize, imgSize);
  };

  render() {
    const {NODE_R} = App;
    const {data, highlightLink} = this.state;

    return <ForceGraph2D
      ref={el => {
        this.fg = el;
      }}
      graphData={data}
      cooldownTime={Infinity}
      d3AlphaDecay={0.01}
      d3VelocityDecay={0.1}
      nodeRelSize={NODE_R}
      nodeLabel={"id"}
      nodeCanvasObject={this._paintNode}
      linkDirectionalParticles={"value"}
      // linkDirectionalParticleSpeed={l => l === highlightLink ? l.value * .01 : 0}
      linkDirectionalParticleSpeed={l => l.value * .001}
      // linkDirectionalParticleWidth={l => l === highlightLink ? 4 : 0}
      linkDirectionalParticleWidth={4}
      linkWidth={l => l === highlightLink ? 5 : 1}
      linkCurvature={0}
      linkCurveRotation={0}
      onNodeClick={onNodeClick}
      onNodeDragEnd={onNodeDragEnd}
      onNodeHover={this._handleNodeHover}
      onLinkHover={this._handleLinkHover}
    />
  }
}

export default App;
